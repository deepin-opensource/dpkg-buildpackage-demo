# dpkg-buildpackage 打包示例

## 通过自定义脚本构建程序并复制到指定文件夹

### 项目

[custom_script_build_and_copy](custom_script_build_and_copy)

### 构建

```
git clone https://gitee.com/SwimmingTiger/dpkg-buildpackage-demo.git
cd dpkg-buildpackage-demo/custom_script_build_and_copy
dpkg-buildpackage
```

备注：构建命令在最后阶段会失败，因为你没有我的GPG私钥，无法以我的身份为包生成GPG签名，但是这不影响你使用构建出来的包。构建实际上已经成功，只是没有GPG签名而已。

### 清理

在 `dpkg-buildpackage-demo/custom_script_build_and_copy` 中执行：

```
debclean
```

## 许可

该版本库中的所有内容均属于公有领域，请随意使用。
